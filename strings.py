s = 'строка'
print(len(s))
number = 100500
u = str(number)
print(s * 3)
print(s + ' ' + u)


s = 'abcdefg'
print(s[1])
print(s[-1])
print(s[1:3])
print(s[1:-1])
print(s[:3])
print(s[2:])
print(s[:-1])
print(s[::2])
print(s[1::2])
print(s[::-1])

s = 'abcdefghijklm'
print(s[0:10:2])
for i in range(0, 10, 2):
    print(i, s[i])

my_string = 'Hello'
print(my_string.find('e'))
# вернёт 1
print(my_string.find('ll'))
# вернёт 2
print(my_string.find('L'))
# вернёт -1

S = 'Hello'
print(S.find('l'))
# вернёт 2
print(S.rfind('l'))
# вернёт 3

print('Hello'.replace('l', 'L'))
print('Abrakadabra'.replace('a', 'A', 2))

print('Abracadabra'.count('a'))
# вернёт 4
print(('a' * 10).count('aa'))
# вернёт 5


print('как же это все тошно'.lower())
print('как же это все тошно'.upper())
print('как же это все тошно. правда?'.capitalize())
print('а'.isalpha())
print('б'.isalpha())
print(' '.isalpha())
print('!'.isalpha())
print('1'.isalpha())
print('1'.isalpha())
print('abcd'.isalpha())
print('abcd123'.isalpha())
