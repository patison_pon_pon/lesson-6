# LEGB
# my_var = 'Внешняя'
# other_var = 'Другая внешняя'
#
#
# def print_my_var():
#     # print(my_var)
#     print(other_var)
#     my_var = 'Внутренняя'
#     print(my_var)
#
#
#
# print_my_var()
# print(my_var)


# my_var = 'Внешняя'
#
#
# def print_my_var():
#     my_var = 'Внутренняя'
#
#     def inner_print_my_var():
#         my_var = 'Внутренняя - Внутренняя'
#         print(my_var)
#
#     inner_print_my_var()
#
#
#
# print_my_var()


# my_var = 'Внешняя'
#
#
# def print_my_var():
#     my_var = 'Внутренняя'
#
#     def inner_print_my_var():
#         global my_var
#         print(my_var)
#         my_var = 'Внутренняя - Внутренняя'
#         print(my_var)
#
#     inner_print_my_var()
#     print(my_var)
#
#
#
# print_my_var()
# print(my_var)


my_var = 'Внешняя'


def print_my_var():
    my_var = 'Внутренняя'

    def inner_print_my_var():
        nonlocal my_var
        print(my_var)
        my_var = 'Внутренняя - Внутренняя'
        print(my_var)

    inner_print_my_var()
    print(my_var)



print_my_var()
print(my_var)
