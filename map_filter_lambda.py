def sum_of_two_numbers(x):
    return x + x


number_list = list(range(1, 11))
result = map(sum_of_two_numbers, number_list)
print(result)

for item in result:
    print(item)

for item in map(sum_of_two_numbers, number_list):
    print(item)

result = map(sum_of_two_numbers, number_list)
print(result)
print(list(result))
print(result)
print(list(result))
print(list(map(sum_of_two_numbers, number_list)))


def map_func_with_multiple_args(a, b, c):
    return a + b + c


list_1 = [2, 4, 8, 16]
list_2 = [2, 4, 8, 16]
list_3 = [2, 4, 8, 16, 32, 64]
print(list(map(map_func_with_multiple_args, list_1, list_2, list_3)))


def is_number_even(number):
    return number % 2 == 0


print(list(filter(is_number_even, range(1, 11))))

even_numbers = list()
for number in range(1, 11):
    if number % 2 == 0:
        even_numbers.append(number)
print(even_numbers)

# lambda - анонимная функция. Она используется только один раз, во время ее создания.
# У нее нет имени и мы ее больше не можем использовать


def cube(number):
    return number ** 3


number_list = list(range(1, 11))
print(list(map(cube, number_list)))


# def cube(number): return number ** 3
number_list = list(range(1, 11))
print(list(map(lambda number: number ** 3, number_list)))
print(list(filter(lambda number: number % 2 == 0, range(1, 11))))
print(list(filter(lambda number: number % 2 == 1, range(1, 11))))
