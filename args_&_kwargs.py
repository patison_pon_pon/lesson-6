def ten_percent_of_product(x, y, z):
    return (x * y * z) * 0.1


# print(ten_percent_of_product(10, 20, 7, 2))
print(ten_percent_of_product(10, 20, 7))


def ten_percent_of_product_with_args(*args):
    print(args)
    print(type(args))
    result = 1
    for number in args:
        result = result * number
    return result * 0.1


print(ten_percent_of_product_with_args(10, 20, 7, 2, 345))


def ten_percent_of_product_with_args_and_arg(percent, *args):
    print(args)
    print(type(args))
    result = 1
    for number in args:
        result = result * number
    return result / 100 * percent


print(ten_percent_of_product_with_args_and_arg(20, 10, 20, 7, 2, 345))
print(ten_percent_of_product_with_args_and_arg(20, 10, 20, 7, 2, 345))

my_first_list = [1, 2, 3]
my_second_list = [4, 5, 6]
my_merged_list = [*my_first_list, *my_second_list]
print(my_merged_list)
print(ten_percent_of_product_with_args_and_arg(20, *my_merged_list))


def func_with_kwargs(**kwargs):
    print(kwargs)
    print(type(kwargs))


func_with_kwargs(a=1, b=2, c=3)


def hello_with_kwargs(**kwargs):
    if 'name' in kwargs:
        print(f"Здарова, {kwargs['name']}! Как твои дела?")
    else:
        print('Ты кто такой?')


hello_with_kwargs(gender='male', age=24)
hello_with_kwargs(gender='male', age=24, name='Колян')


def hello_with_greeting_and_kwargs(greeting, **kwargs):
    if 'name' in kwargs:
        print(f"{greeting}, {kwargs['name']}! Как твои дела?")
    else:
        print(f'{greeting}! Ты кто такой?')


hello_with_greeting_and_kwargs('Ку', gender='male', age=24)
hello_with_greeting_and_kwargs('Ку', gender='male', age=24, name='Колян')


def func_with_args_and_kwargs(*args, **kwargs):
    print(args)
    print(kwargs)
    print(f'I want {args[0]} {kwargs["drink"]} and {args[1]} {kwargs["food"]}')


func_with_args_and_kwargs(1, 2, drink='coffee', food='snickers')


bottles_catalog = {
    "буратино": 2.0,
    "burn": 1.5,
    "fanta": 2.5,
    "pepsi": 2.3,
    "боржоми": 1.3,
    "колокольчик": 3.0,
}


def bottles_sum(name, bottles, default_price=1, **kwargs):
    exclusive_result = 0
    for bottle, quantity in kwargs.items():
        if bottle in bottles_catalog:
            exclusive_result += quantity * bottles_catalog[bottle]
            bottles -= quantity
    result = exclusive_result + bottles * default_price
    print(f"{name} насобирал бутылок на {result} грн. Из них эксклюзива на {exclusive_result} грн")


name = "Игорёк Синеватый"
bottles_sum(name, 25, буратино=3, fanta=4, колокольчик=1)



